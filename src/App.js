import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

import './App.css';
import 'bulma/css/bulma.css';

import { Home } from './pages/Home';
import { Details } from './pages/Details';
import { NotFound } from './pages/NotFound';


class App extends Component {
  render(){
    
    return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Home}></Route>
          <Route path='/detail/:movieID' component={Details}></Route>
          <Route component={NotFound}></Route>
        </Switch>
      </div>
    )
  }
}

export default App;
