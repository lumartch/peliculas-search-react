import React, {Component} from 'react';
import { Title } from '../components/title';
import { SearchForm }  from '../components/searchForm';
import { MovieList } from '../components/MoviesList';

export class Home extends Component {
    state = { usedSearch:false, results: [] }

    _handleResults = ( results ) => {
        this.setState({ results, usedSearch:true })
      }
    
      _renderResults() {
        return this.state.results.length === 0 
        ? <p> ¡Sin resultados! D:</p>
        : <MovieList movies={this.state.results}></MovieList>
      }
    

    render() {
        return(
            <div>
                <Title>Películas</Title>
                <div className='SearchForm-wrapper'>
                <SearchForm onResults={this._handleResults}></SearchForm>
                </div>
                {
                this.state.usedSearch
                ? this._renderResults()
                : <small> Use el buscador para películas</small>
                }
            </div>
        )
    }
}