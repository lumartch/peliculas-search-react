import React,  { Component } from 'react';
import PropTypes from 'prop-types';

import { ButtonBackHome } from '../components/ButtonBackHome';

const API_KEY = ''

export class Details extends Component {
    static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.object,
            isExact: PropTypes.bool,
            path: PropTypes.string,
            url: PropTypes.string
        })
    }

    state = { movie: {} }

    _fetchMovie({ id }){
        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&i=${id}`)
        .then(res => res.json())
        .then(movie => {
            console.log({movie})
            this.setState({movie})
        })
    }

    componentDidMount(){
       const { movieID } = this.props.match.params
       this._fetchMovie({id :movieID}) 
    }

    _goBack(){
        window.history.back()
    }

    render() {
        const {Title, Poster, Metascore, Plot} = this.state.movie
        return (
            <div>
                <ButtonBackHome></ButtonBackHome>
                <h1>{Title}</h1>
                <img src={Poster}></img>
                <h3>{Metascore}</h3>
                <p>{Plot}</p>
            </div>
        )
    }
}